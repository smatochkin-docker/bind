# BIND DNS server Docker images

## Create images

```
docker build -t smatochkin/bind:base .
docker build -t smatochkin/bind:foo samples/foo
```

## Use DNS server

```
docker run -d -p 53:53/udp -p 53:53 smatochkin/bind:foo
```

## Manage DNS server with rndc


```
docker run -d --name foo_dns -p 53:53/udp -p 53:53 smatochkin/bind:foo
docker run --rm --volumes-from foo_dns --link foo_dns:bind smatochkin/bind:foo sh -c 'rndc -s $BIND_PORT_953_TCP_ADDR sync'
```

or

```
docker run --rm --volumes-from foo_dns --link foo_dns:bind smatochkin/bind:foo rndc -s $(docker inspect -f '{{.NetworkSettings.IPAddress}}' foo_dns) sync
```

## Update RRs using dynamic updates

```
docker run -i --rm --volumes-from foo_dns smatochkin/bind:foo nsupdate -k /etc/bind/named.conf.keys <<EOF
server $(docker inspect -f '{{.NetworkSettings.IPAddress}}' foo_dns)
update add one.foo.tld 600 A 127.0.0.1
send
EOF
```

FROM phusion/baseimage:0.9.12
MAINTAINER sergey@matochkin.com

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
      bind9 \
      bind9-host \
      dnsutils && \
    apt-get clean

RUN install -d -o bind -g bind -m 755 /var/run/named

EXPOSE 53/udp 53 953

CMD ["/usr/sbin/named", "-u", "bind", "-g"]

# Usage:
# To upload custom configuration to BIND server
# create ./etc/bind directory next to your Dockerfile.
# Put your named.conf and other configuration files
# there. And build your project using the following
# Dockerfile
# FROM smatochkin/bind:latest
# MAINTAINER <your@email.address.tld>
#
ONBUILD RUN rm /etc/bind/*
ONBUILD ADD etc/bind/ /etc/bind/
ONBUILD RUN chown bind:bind -R /etc/bind/
ONBUILD VOLUME /etc/bind
